import Vue from 'vue'
import App from './App.vue'

import router from './Router.js';
import TitleMixin from './mixins/title.js'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebook, faTwitter, faInstagram, faYoutube, faGooglePlus, faLinkedin, faPinterest } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

library.add(faFacebook, faTwitter, faInstagram, faYoutube, faGooglePlus, faLinkedin, faPinterest);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.mixin(TitleMixin);

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue';

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
